

package es.esy.abody.articleServer.sharedVOs;

/**
 *
 * @author HussinMahmoud
 */
public class DepartmentVO {
    private int id ;
    private String title ;
    private String desc ;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }
    
    
    
    
    
}
