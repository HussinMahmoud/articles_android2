/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.esy.abody.articleServer.sharedVOs;

/**
 *
 * @author Fujitsju
 */
public class ArticleVO {

    private int id;
    private String title;
    private String desc = "";
    private String content = "";
    private int author_id;
    private String author_name;
    private int department_id;
    private String department_title;
    private String photo_url;
    private String created;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public String getContent() {
        return content;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public String getDepartment_title() {
        return department_title;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public String getCreated() {
        return created;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public void setDepartment_title(String department_title) {
        this.department_title = department_title;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
