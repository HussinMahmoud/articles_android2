package es.esy.abody.articleServer;

/**
 * Created by HussinMahmoud on 04/03/2015.
 */

import android.util.Log;
import com.google.gson.Gson;
import es.esy.abody.articleServer.httpRequest.*;
import es.esy.abody.articleServer.responses.*;


public class ArticleServer {

    public final String SITE_URL = "http://abody.esy.es/article_app/fetches/";
    public final String KEYHASH = "1JT5Q66g0N2jwEPadr2FScbGKmhMEIHnkG7N3X1ghxtBL9upXyTAYEN6mdS46RX";
    public static final int     CONNECTION_TIMEOUT  = 30 * 1000;
    private String uidClient;
    private HttpClientArticle httpClientConnction;
    private HttpResponseVO response;
    private Gson gson;

    public ArticleServer() {
        this.httpClientConnction = new HttpClientArticle();
        this.response = new HttpResponseVO();
        this.gson = new Gson();

    }

    public void setUidClient(String uidClient) {
        this.uidClient = uidClient;
    }

    public FetchAll fristRunApp() {
        String urlrequest = this.SITE_URL + "fetch_start/" + this.KEYHASH + "/" + this.uidClient + ".json";
        FetchAll resultFetch = null;
        this.response = null;
        try {
            this.response = this.httpClientConnction.get(urlrequest,null,CONNECTION_TIMEOUT);
            resultFetch = this.gson.fromJson(this.response.responseBody, FetchAll.class);


        } catch (Exception ex) {
            Log.d("WRONG : ",ex.toString());
        }
        return resultFetch;
    }

    public FetchAll updateAppData() {
        String urlrequest = this.SITE_URL + "fetch_update/" + this.uidClient + ".json";
        FetchAll resultFetch = null;
        this.response = null;
        try {
            this.response = this.httpClientConnction.get(urlrequest,null,CONNECTION_TIMEOUT);
            resultFetch = this.gson.fromJson(this.response.responseBody, FetchAll.class);


        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        return resultFetch;
    }
/*
fetchArticlesOlder    article id ,0,0
fetchArticlesOlderBarDepartment  article id ,Depaetment_id,0
fetchArticlesOlderBarAuthor    article id ,0,author_id


 */

    public FetchArticles fetchArticlesOlder(int article_id, int depaetment_id, int author_id) {

        String urlrequest = null;
        if (depaetment_id == 0 && author_id == 0) {  // to fetch older then article without filter
            urlrequest = this.SITE_URL + "fetch_older_articles/" + this.uidClient + "/" + article_id + "/0/0.json";
        } else if (depaetment_id != 0) {  // to fetch older then article bar department id
            urlrequest = this.SITE_URL + "fetch_older_articles/" + this.uidClient + "/" + article_id + "/" + depaetment_id + "/0.json";
        } else if (author_id != 0) {  // to fetch older then article bar author id
            urlrequest = this.SITE_URL + "fetch_older_articles/" + this.uidClient + "/" + article_id + "/0/" + author_id + ".json";
        }
        FetchArticles resultFetch = null;
        this.response = null;

        try {
            this.response = this.httpClientConnction.get(urlrequest,null,CONNECTION_TIMEOUT);
            resultFetch = this.gson.fromJson(this.response.responseBody, FetchArticles.class);

        } catch (Exception ex) {
// will add logger  her
        }
        return resultFetch;

    }

    public FetchOneArticle fetchSelectArticle(int article_id) {
        String urlrequest = this.SITE_URL + "fetch_article_by_id/" + this.uidClient + "/" + article_id + ".json";
        FetchOneArticle resultFetch = null;
        this.response = null;
        try {
            this.response = this.httpClientConnction.get(urlrequest,null,CONNECTION_TIMEOUT);;
            resultFetch = this.gson.fromJson(this.response.responseBody, FetchOneArticle.class);

        } catch (Exception ex) {

        }
        return resultFetch;
    }


}
