package es.esy.abody.articleServer.httpRequest;

/**
 * Created by HussinMahmoud on 12/03/2015.
 */

import es.esy.abody.utils.Utils;
import  es.esy.abody.articleServer.httpRequest.HttpResponseVO;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
public class HttpClientArticle {

    public HttpResponseVO get(String urlString, Hashtable<String, String> headers, int timeOutMs) throws Exception {

        HttpResponseVO httpResponseVO = new HttpResponseVO();

        URL myURL = new URL(urlString);

        HttpURLConnection urlConnection = (HttpURLConnection)myURL.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setConnectTimeout(timeOutMs);

        if(headers != null){
            for (String key : headers.keySet()) {
                urlConnection.setRequestProperty(key, headers.get(key));
            }
        }

        httpResponseVO.responseCode = urlConnection.getResponseCode();

        try{
            httpResponseVO.responseBody = Utils.streamToString(urlConnection.getInputStream());
        } catch (FileNotFoundException e){
            httpResponseVO.responseBody = Utils.streamToString(urlConnection.getErrorStream());
        }

        return httpResponseVO;
    }
}










