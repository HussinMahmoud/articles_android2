/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package es.esy.abody.articleServer.responses;

import java.util.ArrayList;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articleServer.sharedVOs.AuthorVO ;
import es.esy.abody.articleServer.sharedVOs.DepartmentVO;
/**
 *
 * @author HussinMahmoud
 */
public class FetchAll {
    private ArrayList <DepartmentVO> departments = null;
    private ArrayList <AuthorVO> authors = null ;
    private ArrayList <ArticleVO> articles = null;

    
    
    
    
    public ArrayList<DepartmentVO> getDepartments() {
        return departments;
    }

    public ArrayList<AuthorVO> getAuthors() {
        return authors;
    }

    public ArrayList<ArticleVO> getArticles() {
        return articles;
    }
    
    
    
}
