package es.esy.abody.broadcast;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.Bundle;


public class HttpClientArticals {
	
	
	
	private HttpClient httpClient = new DefaultHttpClient();
	private HttpRequestBase request;
	private HttpResponse response = null;




	/*************** ACTIONS *****************/
	public String signIn(String email, String password ,String url) {
		JSONObject js = null;
		String resultUser = "";

		try {
		//	String url = Api.getSignInUrl();
			List<NameValuePair> data = new ArrayList<NameValuePair>(2);
			data.add(new BasicNameValuePair("email", email));
			data.add(new BasicNameValuePair("password", password));

			String result = executePost(url, data, null);

			js = new JSONObject(result);
			JSONObject jsUser = js.getJSONObject("user");

	//		AppData.getInstance().profileUser.loadFromJSON(jsUser);
	//		AppData.getInstance().profileUser.setPassword(password);

			resultUser = jsUser.getString("authentication_token");

		} catch (Exception e) {

			e.printStackTrace();
		}
		return resultUser;
	}

	

	/*********** END OF ACTIONS **************/

	/************** HTTP Operations ******************/
	private String executePost(String url, List<NameValuePair> data,
			JSONObject jsonData) {
		String resultResponse = "";
		try {

			request = new HttpPost();
			request.setURI(new URI(url));
//			request.addHeader(
//					"Authorization",
//					"Token token=\""
//							+ AppData.getInstance().profileUser
//									.getAuthenticationToken() + "\"");//

			if (data == null) {
				StringEntity se = new StringEntity(jsonData.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				((HttpPost) request).setHeader("Accept", "application/json");
				((HttpPost) request).setHeader("Content-type",
						"application/json");
				((HttpPost) request).setEntity(se);
			} else
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(data));
			response = httpClient.execute(request);

			HttpEntity httpEntity = response.getEntity();

			resultResponse = EntityUtils.toString(httpEntity);

			return resultResponse;

		} catch (Exception e) {

			e.printStackTrace();
		}

		return resultResponse;
	}

	public String executePut(String url, List<NameValuePair> data,
			JSONObject jsonData) {
		String resultResponse = "";
		try {

			request = new HttpPut();
			request.setURI(new URI(url));
//			request.addHeader(
//					"Authorization",
//					"Token token=\""
//							+ AppData.getInstance().profileUser
//									.getAuthenticationToken() + "\"");//

			if (data == null) {
				StringEntity se = new StringEntity(jsonData.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				((HttpPut) request).setHeader("Accept", "application/json");
				((HttpPut) request).setHeader("Content-type",
						"application/json");
				((HttpPut) request).setEntity(se);
			} else
				((HttpPut) request).setEntity(new UrlEncodedFormEntity(data));
			response = httpClient.execute(request);

			HttpEntity httpEntity = response.getEntity();

			resultResponse = EntityUtils.toString(httpEntity);

			return resultResponse;

		} catch (Exception e) {

			e.printStackTrace();
		}

		return resultResponse;
	}

	public String executeGet(String url, Bundle data) {

		String resultResponse = "";
		try {
			request = new HttpGet();
			request.setURI(new URI(url));

//			request.addHeader(
//					"Authorization",
//					"Token token=\""
//							+ AppData.getInstance().profileUser
//									.getAuthenticationToken() + "\"");

			response = httpClient.execute(request);
			HttpEntity httpEntity = response.getEntity();
			resultResponse = EntityUtils.toString(httpEntity);

		} catch (Exception e) {

			e.printStackTrace();
		}
		return resultResponse;
	}

//	private void sendNetworkStatus(boolean isOnline) {
//		Intent intentResponse = new Intent();
//		intentResponse.setAction(Actions.UPDATE_NETWORK_STATUS);
//		intentResponse.putExtra("isOnline", isOnline);
//		intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
//		sendBroadcast(intentResponse);
//	}

}
