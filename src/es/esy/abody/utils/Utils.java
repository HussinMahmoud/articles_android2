package es.esy.abody.utils ;

import java.io.IOException;

public class Utils {

    public static String getRandomString(int stringLength){
        StringBuilder str = new StringBuilder(stringLength);

        for(int i = 0; i < stringLength; i++){
            str.append((char )('a' + Math.random() * ('z' - 'a') ));
        }
        return str.toString();
    }

    public static String streamToString(java.io.InputStream is) throws IOException {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        String str = s.hasNext() ? s.next() : "";
        is.close();

        return str;
    }

}
