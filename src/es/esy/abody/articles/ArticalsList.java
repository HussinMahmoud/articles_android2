package es.esy.abody.articles;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

@SuppressLint("InflateParams")
public class ArticalsList extends Activity {

	private Spinner spinnerArticals;
	private LinearLayout lLayout;
	private LayoutInflater inflater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.articals_list);

		spinnerArticals = (Spinner) this.findViewById(R.id.spinner1);
		inflater = (LayoutInflater) this
				.getSystemService(LAYOUT_INFLATER_SERVICE);

		addItemsOnSpinner();
		lLayout = (LinearLayout) this.findViewById(R.id.list_scroll);
		fillListData();

	}

	private void fillListData() {
		for (int i = 0; i < 50; i++) {

			View partial = inflater.inflate(R.layout.artical_row_partial, null);
			partial.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					 Intent i = new Intent(v.getContext(),ArticalDetails.class);
					 startActivity(i);
				

				}
			});

			lLayout.addView(partial);

		}
	}

	private void addItemsOnSpinner() {

		List<String> list = new ArrayList<String>();

		list.add("Categories");

		list.add("Item 2");

		list.add("Item 3");

		list.add("Item 4");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_checked, list);

		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_list_item_checked);

		spinnerArticals.setAdapter(dataAdapter);
		spinnerArticals.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

	}

}
