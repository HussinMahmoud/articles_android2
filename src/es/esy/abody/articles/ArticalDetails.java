package es.esy.abody.articles;

import java.util.zip.Inflater;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import es.esy.abody.broadcast.HttpClientArticals;

public class ArticalDetails extends Activity {

	private TextView title,content,date,auther;
	private String path ="http://abody.esy.es/article_app/fetches/fetch_article_by_id/s850HussinMahmoud/1.json";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.artical_details);
		title=(TextView) findViewById(R.id.title);
		content=(TextView) findViewById(R.id.content);
		date=(TextView) findViewById(R.id.date);
		auther=(TextView) findViewById(R.id.auther);
		new GetData().execute();
	}

	private class GetData extends AsyncTask<Inflater, Void, Void> {

		private ProgressDialog pDialog;
        private String resultData ="";
	

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(ArticalDetails.this);
			pDialog.setTitle("Loading");
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();

		}

		
		@Override
		protected Void doInBackground(Inflater... params) {

			HttpClientArticals httpclient = new HttpClientArticals();
			  resultData =httpclient.executeGet(path, null);
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			pDialog.dismiss();
			 try {
				JSONObject jObject=new JSONObject(resultData);
				JSONObject articalJson= jObject.getJSONObject("article");
				title.setText(articalJson.getString("title"));
				content.setText(articalJson.getString("content"));
				auther.setText(articalJson.getString("author_name"));
				date.setText(articalJson.getString("created"));
				
			} catch (JSONException e) {
			
				e.printStackTrace();
			}
		}
	}

}
