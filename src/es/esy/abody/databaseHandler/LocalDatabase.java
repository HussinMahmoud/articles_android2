package es.esy.abody.databaseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articleServer.sharedVOs.AuthorVO;
import es.esy.abody.articleServer.sharedVOs.DepartmentVO;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HussinMahmoud on 05/03/2015.
 */

public class LocalDatabase extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "articles";

    //Setting Table Columns
    private static final String TABLE_SETTING ="Setting";
    private static final String SETTING_KEY_UID = "uid";


   // Department Table Columns names
    private static final String TABLE_DEPARTMENT ="Department";
    private static final String DEPARTMENT_KEY_ID = "id";
    private static final String DEPARTMENT_KEY_TITLE = "title";
    private static final String DEPARTMENT_KEY_DESC = "desc";
    // Article Table Columns names
    private static final String TABLE_ARTICLE ="Article";
    private static final String ARTICLE_KEY_ID = "id";
    private static final String ARTICLE_KEY_TITLE = "title";
    private static final String ARTICLE_KEY_DESC = "desc";
    private static final String ARTICLE_KEY_CONTENT = "content";
    private static final String ARTICLE_KEY_AUTHOR_ID = "author_id";
    private static final String ARTICLE_KEY_AUTHOR_NAME = "author_name";
    private static final String ARTICLE_KEY_DEPARTMENT_ID = "department_id";
    private static final String ARTICLE_KEY_DEPARTMENT_TITELE = "department_title";
    private static final String ARTICLE_KEY_PHOTO_URL = "photo_url";
    private static final String ARTICLE_KEY_CREATED = "created";
    // Author Table Columns names
    private static final String TABLE_AUTHOR ="Author";
    private static final String AUTHOR_KEY_ID = "id";
    private static final String AUTHOR_KEY_NAME = "name";




//Constructor
   public LocalDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createDepartmentTable = "CREATE TABLE " + TABLE_DEPARTMENT + "("
                + DEPARTMENT_KEY_ID + " INTEGER," + DEPARTMENT_KEY_TITLE + " TEXT,"
                + DEPARTMENT_KEY_DESC + " TEXT" + ")";
        String createAuthorTable = "CREATE TABLE " + TABLE_AUTHOR + "("
                + AUTHOR_KEY_ID + " INTEGER," + AUTHOR_KEY_NAME + " TEXT" + ")";

        String createArticleTable = "CREATE TABLE " + TABLE_ARTICLE + "("
                + ARTICLE_KEY_ID + " INTEGER," +
                ARTICLE_KEY_TITLE + " TEXT,"+
                ARTICLE_KEY_DESC + " TEXT," +
                ARTICLE_KEY_CONTENT + " TEXT," +
                ARTICLE_KEY_AUTHOR_ID + " INTEGER," +
                ARTICLE_KEY_AUTHOR_NAME + " TEXT," +
                ARTICLE_KEY_DEPARTMENT_ID + " INTEGER," +
                ARTICLE_KEY_DEPARTMENT_TITELE + " TEXT,"
                + ARTICLE_KEY_PHOTO_URL + " TEXT,"+
                ARTICLE_KEY_CREATED + " TEXT" +")";

        String createSettingTable = "CREATE TABLE " + TABLE_SETTING + "("+
                 SETTING_KEY_UID + " TEXT" + ")";

        db.execSQL(createDepartmentTable);
        db.execSQL(createAuthorTable);
        db.execSQL(createArticleTable);
        db.execSQL(createSettingTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
// Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DEPARTMENT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTHOR);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTING);

// Create tables again
        onCreate(sqLiteDatabase);
    }
//##################################################DEPARTMENTS TABLE#################################
    // Adding new Department
   public void addDepartment(DepartmentVO Department) throws UnsupportedEncodingException {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DEPARTMENT_KEY_ID, Department.getId()); // Department Id
        values.put(DEPARTMENT_KEY_TITLE, URLEncoder.encode(Department.getTitle(), "UTF-8")); // Department Title
       values.put(DEPARTMENT_KEY_DESC,URLEncoder.encode(Department.getDesc(), "UTF-8")); // Department descrbtion

        // Inserting Row
        db.insert(TABLE_DEPARTMENT, null, values);
        db.close(); // Closing database connection
    }
    // Deleting single Department
    public void deleteDepartment(DepartmentVO Department) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DEPARTMENT, DEPARTMENT_KEY_ID + " = ?",
                new String[] { String.valueOf(Department.getId()) });
        db.close();
    }

    // Getting All Departments
    public  ArrayList<DepartmentVO> getAllDepartments() throws UnsupportedEncodingException {
        ArrayList<DepartmentVO> departmentList = new ArrayList<DepartmentVO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DEPARTMENT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DepartmentVO department = new DepartmentVO();
                department.setId(cursor.getInt(cursor.getColumnIndex(DEPARTMENT_KEY_ID)));
                department.setTitle(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(DEPARTMENT_KEY_TITLE)), "UTF-8"));
                department.setDesc(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(DEPARTMENT_KEY_DESC)), "UTF-8"));

                // Adding contact to list
                departmentList.add(department);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return author list
        return departmentList;
    }

    public boolean isFoundDepartmentById (int department_id) throws UnsupportedEncodingException {

        String selectQuery = "SELECT  * FROM " + TABLE_DEPARTMENT + " WHERE "+DEPARTMENT_KEY_ID +"="+department_id ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
        	cursor.close();
        	db.close();
          return true ;

        }
        cursor.close();
        db.close();
        return false ;

    }


    //##################################################AUTHORS TABLE#################################
    // Adding new Author
    public void addAuthor(AuthorVO Author) throws UnsupportedEncodingException {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(AUTHOR_KEY_ID, Author.getId()); // Author Id
        values.put(AUTHOR_KEY_NAME,  URLEncoder.encode(Author.getName(), "UTF-8")); // Author Name

        // Inserting Row
        db.insert(TABLE_AUTHOR, null, values);
        db.close(); // Closing database connection
    }


    // Deleting single Author
    public void deleteAuthor(AuthorVO Author) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_AUTHOR, AUTHOR_KEY_ID + " = ?",
                new String[] { String.valueOf(Author.getId()) });
        db.close();
    }

    // Getting All Authors
    public ArrayList<AuthorVO> getAllAuthors() throws UnsupportedEncodingException {
        ArrayList<AuthorVO> AuthorList = new ArrayList<AuthorVO>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_AUTHOR;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AuthorVO author = new AuthorVO();
                author.setId(cursor.getInt(cursor.getColumnIndex(AUTHOR_KEY_ID)));
                author.setName(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(AUTHOR_KEY_NAME)),"UTF-8"));

                // Adding contact to list
                AuthorList.add(author);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return author list
        return AuthorList;
    }

    public boolean isFoundAuthorById (int author_id) throws UnsupportedEncodingException {

        String selectQuery = "SELECT  * FROM " + TABLE_AUTHOR + " WHERE "+AUTHOR_KEY_ID +"="+author_id ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
        	cursor.close();
        	db.close();
            return true ;

        }
        cursor.close();
        db.close();
        return false ;

    }

    //##################################################ARTICLES TABLE#################################
    // Adding new Article
    public void addArticle(ArticleVO Article) throws UnsupportedEncodingException {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ARTICLE_KEY_ID, Article.getId()); // Article Id
        values.put(ARTICLE_KEY_TITLE, URLEncoder.encode(Article.getTitle(), "UTF-8")); // Article Title
        values.put(ARTICLE_KEY_DESC, URLEncoder.encode(Article.getDesc(), "UTF-8")); // Article Desc
       if((Article.getContent().length()==0)) {
          	   values.put(ARTICLE_KEY_CONTENT,"");
       }else{
    
		    values.put(ARTICLE_KEY_CONTENT,URLEncoder.encode( Article.getContent(), "UTF-8")); // Article Content
       }
        values.put(ARTICLE_KEY_AUTHOR_ID, Article.getAuthor_id()); // Article Author id
        values.put(ARTICLE_KEY_AUTHOR_NAME, URLEncoder.encode( Article.getAuthor_name(), "UTF-8") ); // Article Author name
        values.put(ARTICLE_KEY_DEPARTMENT_ID, Article.getDepartment_id()); // Article Department id
        values.put(ARTICLE_KEY_DEPARTMENT_TITELE,URLEncoder.encode( Article.getDepartment_title(), "UTF-8") ); // Article Department title
        values.put(ARTICLE_KEY_PHOTO_URL,URLEncoder.encode(  Article.getPhoto_url(), "UTF-8")); //Photo_url
        values.put(ARTICLE_KEY_CREATED, URLEncoder.encode( Article.getCreated(), "UTF-8")); // Article Created

        // Inserting Row
        db.insert(TABLE_ARTICLE, null, values);
        db.close(); // Closing database connection
    }

    // Deleting single Article




    public void deleteArticle(ArticleVO Article) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ARTICLE, ARTICLE_KEY_ID + " = ?",
                new String[] { String.valueOf(Article.getId()) });
        db.close();
    }


    // (0,0)to get all article
    //(department_id ,0 ) to get all article by department_id
    //(0 ,author_id ) to get all article by author_id
    public ArrayList<ArticleVO> getArticleAllOrByDepartmentIdOrByAuthorId(int department_id ,int author_id ) throws UnsupportedEncodingException {
        ArrayList<ArticleVO> articleList = new ArrayList<ArticleVO>();
        // Select All Query
        String conditions = "";

       if(department_id == 0 && author_id ==0){
           conditions = "";
       }else if(department_id == 0){
            conditions = " WHERE "+ ARTICLE_KEY_AUTHOR_ID +"="+author_id;
        } else if (author_id ==0){
            conditions = " WHERE "+ ARTICLE_KEY_DEPARTMENT_ID +"="+department_id;
        }

        String selectQuery = "SELECT  * FROM " + TABLE_ARTICLE + conditions ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ArticleVO article = new ArticleVO();
                article.setId(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_ID)));
                article.setTitle(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_TITLE)), "UTF-8"));
                article.setDesc(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_DESC)), "UTF-8"));
                article.setAuthor_id(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_AUTHOR_ID)));
                article.setAuthor_name(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_AUTHOR_NAME)), "UTF-8"));
                article.setDepartment_id(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_DEPARTMENT_ID)));
                article.setDepartment_title(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_DEPARTMENT_TITELE)), "UTF-8"));
                article.setPhoto_url(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_PHOTO_URL)), "UTF-8"));
                article.setCreated(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_CREATED)), "UTF-8"));

                // Adding contact to list
                articleList.add(article);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        

        // return author list
        return articleList;
    }

    // get article by id
    public ArticleVO getArticleById (int article_id) throws UnsupportedEncodingException {

        String selectQuery = "SELECT  * FROM " + TABLE_ARTICLE + " WHERE "+ARTICLE_KEY_ID +"="+article_id ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArticleVO article = new ArticleVO();

        if (cursor.moveToFirst()) {
            article.setId(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_ID)));
            article.setTitle(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_TITLE)), "UTF-8"));
            article.setDesc(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_DESC)), "UTF-8"));
            article.setAuthor_id(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_AUTHOR_ID)));
            article.setAuthor_name(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_AUTHOR_NAME)), "UTF-8"));
            article.setDepartment_id(cursor.getInt(cursor.getColumnIndex(ARTICLE_KEY_DEPARTMENT_ID)));
            article.setDepartment_title(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_DEPARTMENT_TITELE)), "UTF-8"));
            article.setPhoto_url(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_PHOTO_URL)), "UTF-8"));
            article.setCreated(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_CREATED)), "UTF-8"));
            article.setContent(URLDecoder.decode(cursor.getString(cursor.getColumnIndex(ARTICLE_KEY_CONTENT)), "UTF-8"));

        }else {
		 cursor.close();
		db.close();
            return  null ;
}

        cursor.close();
		db.close();
		return article;

    }

    public boolean isFoundArticleById (int article_id) throws UnsupportedEncodingException {

        String selectQuery = "SELECT  * FROM " + TABLE_ARTICLE + " WHERE "+ARTICLE_KEY_ID +"="+article_id ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
        	cursor.close();
        	db.close();
            return true ;

        }
        cursor.close();
        db.close();
        return false ;

    }

    //###################################################SETTING TABLE#################################

    // Adding Setting uidClient at first
    public void addUidClient(String uid) throws UnsupportedEncodingException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SETTING_KEY_UID,URLEncoder.encode( uid ,"UTF-8")); //uid client
        // Inserting Row
        db.insert(TABLE_SETTING, null, values);
        db.close(); // Closing database connection
    }

    public String getUidClient () throws UnsupportedEncodingException {
        String uid = null;
        String selectQuery = "SELECT  * FROM " + TABLE_SETTING;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list

        if (cursor.moveToFirst()) {
            uid = URLDecoder.decode(cursor.getString(cursor.getColumnIndex(SETTING_KEY_UID)), "UTF-8");
        }
        cursor.close();
        db.close();
        return uid;
    }

}