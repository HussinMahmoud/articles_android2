package es.esy.abody.articleClient;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import es.esy.abody.articleServer.*;
import es.esy.abody.articleServer.responses.FetchAll;
import es.esy.abody.articleServer.responses.FetchArticles;
import es.esy.abody.articleServer.responses.FetchOneArticle;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articleServer.sharedVOs.AuthorVO;
import es.esy.abody.articleServer.sharedVOs.DepartmentVO;
import es.esy.abody.databaseHandler.LocalDatabase;
import es.esy.abody.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HussinMahmoud on 08/03/2015.
 */
 /*
    server
    local databeas
    platforme get uid

     */
public class ArticleClient {
    private String uidClint;
    private ArticleServer masterServer ;
    private LocalDatabase db ;
//    private Build build;     //Build.HARDWARE+"-"+Build.SERIAL


    public ArticleClient(Context context) throws UnsupportedEncodingException {
       this.db =  new LocalDatabase(context);
        this.masterServer = new ArticleServer();
        if (this.db.getUidClient()== null){
            this.createUidClint();    // create uid client to using next time
        }else {
            this.masterServer.setUidClient(this.db.getUidClient());
            this.uidClint = this.db.getUidClient() ;
        }

    }

    public void createUidClint() throws UnsupportedEncodingException {
        String uid = Build.HARDWARE + "-" +Build.HOST +"-"+  Utils.getRandomString(7);
        this.masterServer.setUidClient(uid);
        this.db.addUidClient(uid);
        this.uidClint = uid ;
        return ;
    }
//################################## Menu ################################################
    public boolean startAppAndInsertFristFetch() throws UnsupportedEncodingException {


        FetchAll responseServer = this.masterServer.fristRunApp();
        if (   responseServer == null) {
            return false;
        }
        ArrayList<DepartmentVO> depatList = responseServer.getDepartments();
        ArrayList<AuthorVO> authorsList = responseServer.getAuthors();
        ArrayList<ArticleVO> articleList = responseServer.getArticles();
        try {
            // insert departments
            for (DepartmentVO department : depatList) {
                this.db.addDepartment(department);
            }
            //insert authors
            for (AuthorVO author : authorsList) {
                this.db.addAuthor(author);
            }
            //insert articles
            for (ArticleVO article : articleList) {
                this.db.addArticle(article);
            }

        } catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
            return false;
        }
        return true;
    }

    public boolean updateAppInsertInDb() throws UnsupportedEncodingException {
        FetchAll responseServer = this.masterServer.updateAppData();
        if (  responseServer == null) {    //i want to handle it
            return false; // not connection
        }

        ArrayList<DepartmentVO> depatList = responseServer.getDepartments();
        ArrayList<AuthorVO> authorsList = responseServer.getAuthors();
        ArrayList<ArticleVO> articleList = responseServer.getArticles();
        int totalSize = depatList.size()+ articleList.size()+ authorsList.size() ;

      if(totalSize ==0){    //i want to handle it  (Can not perform inter condition)
          return false ; // not update
      }


        try {
            // update departments and insert new
            if (depatList.size()!=0){
                for (DepartmentVO department : depatList) {
                    if (this.db.isFoundDepartmentById(department.getId())){
                        this.db.deleteDepartment(department);
                    }
                  this.db.addDepartment(department);
                }
            }

        } catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
        }

        try {
            //update  authors and insert new
            if (authorsList.size()!=0){

                for (AuthorVO author : authorsList) {
                    if (this.db.isFoundAuthorById(author.getId())){
                        this.db.deleteAuthor(author);
                    }
                    this.db.addAuthor(author);
                }
            }

        }catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
        }


           try {
               //insert articles
               if (articleList.size()!=0){

                   for (ArticleVO article : articleList) {
                  if(this.db.isFoundArticleById(article.getId())){
                           this.db.deleteArticle(article);
                       }
                       this.db.addArticle(article);
                   }
               }

           }catch (Exception ex) {
               Log.d("WRONG : ", ex.toString());
           }

        return true ; // the update done
    }

//#################################Article List############################################
//####By Article Newer-Older###################################
    public List<ArticleVO> listArticleOrderByNewer() throws UnsupportedEncodingException {
    return this.db.getArticleAllOrByDepartmentIdOrByAuthorId(0, 0);
    }

    // if this method get true we need to  reload ((--listArticleOrderByNewer()--))
    // if false ---show message no older
    public boolean fetchOlderArticle(int article_id) {
        FetchArticles response = this.masterServer.fetchArticlesOlder(article_id, 0, 0);
        if (response == null) {
            return false;
        }
        ArrayList<ArticleVO> articles = response.getArticles();

        try {
            if (articles.size() == 0) {
                return false;
            }
            for (ArticleVO article : articles) {
                this.db.addArticle(article);
            }

        } catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
        }
        return true;

    }

//####By Department Newer-Older################################
    public List<ArticleVO> listArticleByDpartmetId(int department_id) throws UnsupportedEncodingException {
        return this.db.getArticleAllOrByDepartmentIdOrByAuthorId(department_id, 0);
    }

    // if this method get true we need to  reload ((--listArticleOrderByDpartmetId--))
    // if false ---show message no older
    public boolean fetchOlderArticleInDepartment(int article_id,int department_id){
        FetchArticles response = this.masterServer.fetchArticlesOlder(article_id, department_id, 0);
        if (response == null) {
            return false;
        }
        ArrayList<ArticleVO> articles = response.getArticles();

        try {
            if (articles.size() == 0) {
                return false;
            }
            for (ArticleVO article : articles) {
                this.db.addArticle(article);
            }

        } catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
        }
        return true;

    }


//####By Author Newer-Older####################################
    public List<ArticleVO> listArticleByAuthorId(int author_id) throws UnsupportedEncodingException {
        return  this.db.getArticleAllOrByDepartmentIdOrByAuthorId(0, author_id);

    }

    public boolean  fetchOlderArticleToAuthor(int article_id,int author_id){
        FetchArticles response = this.masterServer.fetchArticlesOlder(article_id, 0, author_id);
        if (response == null) {
            return false;
        }
        ArrayList<ArticleVO> articles = response.getArticles();

        try {
            if (articles.size() == 0) {
                return false;
            }
            for (ArticleVO article : articles) {
                this.db.addArticle(article);
            }

        } catch (Exception ex) {
            Log.d("WRONG : ", ex.toString());
        }
        return true;

    }


//#################################Article Preview#########################################

    public ArticleVO getSelecteArticle (int article_id) throws UnsupportedEncodingException {

        ArticleVO article =  this.db.getArticleById(article_id);
        FetchOneArticle response = null ;
        if(article == null) {
           response = this.masterServer.fetchSelectArticle(article_id);

        }else if (article.getContent().length()==0){
             response = this.masterServer.fetchSelectArticle(article_id);

        }
        if (response == null){
            return  null ;
        }

        if (article != null){
            this.db.deleteArticle(article);
        }

        article = response.getArticle() ;
        this.db.addArticle(article);

        return article ;

    }

//##############################Author List############################################
    public ArrayList<AuthorVO> getListAuthors(){
    	try {
			return (ArrayList<AuthorVO>) this.db.getAllAuthors() ;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
    
     //############################Department List#########################################
    public ArrayList<DepartmentVO> getAllDepartments(){
    	try {
			return (ArrayList<DepartmentVO>) this.db.getAllDepartments() ;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    	
    }



    public LocalDatabase getDb() {
        return db;
    }
}
