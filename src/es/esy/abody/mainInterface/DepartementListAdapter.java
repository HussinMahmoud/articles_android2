package es.esy.abody.mainInterface;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import es.esy.abody.articleServer.sharedVOs.DepartmentVO;
import es.esy.abody.articles.R;

public class DepartementListAdapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<DepartmentVO> departementsList;

	public DepartementListAdapter(Context context,
			ArrayList<DepartmentVO> departementsList) {
		// super(context, R.layout.article_row);
		this.context = context;
		this.departementsList = departementsList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return departementsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return departementsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if(departementsList.get(position).getId() == 0) {
			return null;
		}
		LayoutInflater inflater = LayoutInflater.from(context);
		View rowView = inflater.inflate(R.layout.departement_row, null, true);
		TextView departmentName = (TextView) rowView
				.findViewById(R.id.departement_name);
		departmentName.setText(departementsList.get(position).getTitle());
		TextView departmentDesc = (TextView) rowView.findViewById(R.id.departement_desc);
		departmentDesc.setText(departementsList.get(position).getTitle());
		return rowView;
	}

	public void addDepartement(DepartmentVO departement) {
		departementsList.add(departement);
		this.notifyDataSetChanged();
	}

	public void addDepartements(ArrayList<DepartmentVO> departements) {
		for (DepartmentVO departement : departements) {
			departementsList.add(departement);
		}
		this.notifyDataSetChanged();
	}

	public void removeAllDepartements() {
		departementsList.clear();
		this.notifyDataSetChanged();
	}

}
