package es.esy.abody.mainInterface;

import java.io.UnsupportedEncodingException;
import java.net.URL;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import es.esy.abody.articleClient.ArticleClient;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articles.R;

public class ArticleActivity extends ActionBarActivity {

	ArticleVO currentArticle;
	Toolbar toolbar;
	TextView articleContent;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.article_activity);
		toolbar = (Toolbar) findViewById(R.id.articleToolbar);
		setSupportActionBar(toolbar);
		this.v = findViewById(R.layout.article_activity);
		this.currentArticle = ArticlesTab.CURRENT_ARTICLE;
		TextView articleTitle = (TextView) findViewById(R.id.article_title);
		articleContent = (TextView) findViewById(R.id.article_content);
		articleTitle.setText(currentArticle.getTitle());
		articleContent.setText(currentArticle.getContent());
		final ImageView articleIcon = (ImageView) findViewById(R.id.article_icon);
		new Thread() {
			@Override
			public void run() {
				try {
					URL imageUrl = new URL(currentArticle.getPhoto_url());
					final Bitmap articleImage = BitmapFactory
							.decodeStream(imageUrl.openConnection()
									.getInputStream());
					articleIcon.post(new Runnable() {
						@Override
						public void run() {
							articleIcon.setImageBitmap(articleImage);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	private void animateRefreshButton(boolean startOrStop) {
		MenuItem m = toolbar.getMenu().findItem(R.id.action_refresh);
		if (startOrStop) {
			LayoutInflater inflater = LayoutInflater.from(getBaseContext());
			ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_image,
					null);
			Animation rotation = AnimationUtils.loadAnimation(getBaseContext(),
					R.anim.refresh_animation);
			rotation.setRepeatCount(Animation.INFINITE);
			iv.startAnimation(rotation);
			MenuItemCompat.setActionView(m, iv);
		} else {
			MenuItemCompat.getActionView(m).clearAnimation();
			MenuItemCompat.setActionView(m, null);
		}

	}

	private void getArticleContent() {
		new Thread() {
			@Override
			public void run() {
				try {
					ArticleClient client = new ArticleClient(getBaseContext());
					final ArticleVO article = client
							.getSelecteArticle(currentArticle.getId());
					articleContent.post(new Runnable() {

						@Override
						public void run() {
							if (article != null) {
								String selectedArticleContent = article
										.getContent() == null ? "" : article
										.getContent();
								articleContent.setText(selectedArticleContent);
							}
							animateRefreshButton(false);
						}
					});
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.article_activity_menu, menu);
		animateRefreshButton(true);
		getArticleContent();
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_share:
			Intent intent = new Intent(Intent.ACTION_SEND);

			intent.setType("text/plain");

			intent.putExtra(Intent.EXTRA_TEXT,
					getResources().getString(R.string.website_link));

			intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					getResources().getString(R.string.website_message));

			startActivity(Intent.createChooser(intent, "Share"));
			return true;
		case R.id.action_copy_article:
			android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
			clipboard.setText(currentArticle.getContent());
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					getResources().getString(R.string.article_is_copied))
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

								}
							});
			AlertDialog alert = builder.create();
			alert.show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onPause() {
		super.onPause();
		finish();
	}

	@Override
	public void onStop() {
		super.onStop();
		finish();
	}

}
