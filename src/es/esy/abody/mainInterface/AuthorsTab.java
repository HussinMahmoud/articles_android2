package es.esy.abody.mainInterface;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import es.esy.abody.articleClient.ArticleClient;
import es.esy.abody.articleServer.sharedVOs.AuthorVO;
import es.esy.abody.articles.R;

public class AuthorsTab extends Fragment {

	public static int CURRENT_STATE = 0;
	public static int NO_CONTENT = 0;
	public static int ALL_AUTHORS_DATA = 1;

	public static boolean FETCH_OR_NOT = false;

	private ViewPager pager;
	private Toolbar toolbar;
	private LayoutInflater inflater;
	private View v;
	private AuthorsListAdapter authorsAdapter;
	private ArrayList<AuthorVO> authorsList;

	private boolean isFetching = false;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		this.inflater = inflater;
		View v = inflater.inflate(R.layout.authors_tab, container, false);
		this.v = v;
		authorsList = new ArrayList<AuthorVO>();
		authorsAdapter = new AuthorsListAdapter(v.getContext(), authorsList);
		ListView authList = (ListView) v.findViewById(R.id.authors_list);
		authList.setAdapter(authorsAdapter);
		authList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long arg3) {
				ArticlesTab.CURRENT_AUTHOR_ID = authorsList.get(position)
						.getId();
				ArticlesTab.CURRENT_STATE = ArticlesTab.BY_AUTHOR;
				pager.setCurrentItem(1);
			}

		});
		return v;
	}

	public void startFetching() {
		if (!FETCH_OR_NOT) {
			return;
		}
		ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_image,
				null);
		Animation rotation = AnimationUtils.loadAnimation(v.getContext(),
				R.anim.refresh_animation);
		rotation.setRepeatCount(Animation.INFINITE);
		iv.startAnimation(rotation);
		MenuItem m = toolbar.getMenu().findItem(R.id.action_refresh);
		MenuItemCompat.setActionView(m, iv);
		FetchAuthorsDataTask task = new FetchAuthorsDataTask();
		isFetching = true;
		task.execute("Fetch Library Data");
	}

	public void stopFetching() {
		MenuItem m = toolbar.getMenu().findItem(R.id.action_refresh);
		if (MenuItemCompat.getActionView(m) != null && isFetching) {
			MenuItemCompat.getActionView(m).clearAnimation();
			MenuItemCompat.setActionView(m, null);
			isFetching = false;
		}
	}

	public void setPager(ViewPager pager) {
		this.pager = pager;
	}

	public void setToolbar(Toolbar toolbar) {
		this.toolbar = toolbar;
	}

	public int getContentSize() {
		return + authorsList.size();
	}
	
	class FetchAuthorsDataTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			if (CURRENT_STATE == NO_CONTENT) {
				ArticleClient client;
				try {
					client = new ArticleClient(v.getContext());
					final ArrayList<AuthorVO> authors = client.getListAuthors();
					v.post(new Runnable() {

						@Override
						public void run() {
							authorsAdapter.removeAllAuthors();
							authorsAdapter.addAuthors(authors);
						}
					});
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return null;
		}

		@Override
		protected void onPostExecute(String data) {
			stopFetching();
		}

	}

}
