package es.esy.abody.mainInterface;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import es.esy.abody.articles.R;

public class MainTab extends Fragment implements OnTouchListener,
		OnClickListener {

	private ViewPager pager;
	private Toolbar toolbar;
	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.main_tab, container, false);
		this.v = v;
		ImageButton mostlyVistsed = (ImageButton) v
				.findViewById(R.id.mostly_visted_button);
		ImageButton newestArticles = (ImageButton) v
				.findViewById(R.id.newest_articles_button);
		ImageButton departements = (ImageButton) v
				.findViewById(R.id.departements_button);
		ImageButton authors = (ImageButton) v.findViewById(R.id.authors_button);
		ImageButton aboutUs = (ImageButton) v
				.findViewById(R.id.about_us_button);
		mostlyVistsed.setOnTouchListener(this);
		newestArticles.setOnTouchListener(this);
		departements.setOnTouchListener(this);
		authors.setOnTouchListener(this);
		aboutUs.setOnTouchListener(this);

		mostlyVistsed.setOnClickListener(this);
		newestArticles.setOnClickListener(this);
		departements.setOnClickListener(this);
		authors.setOnClickListener(this);
		aboutUs.setOnClickListener(this);


		return v;
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		ImageButton button = (ImageButton) view.findViewById(view.getId());
		if (event.getAction() == MotionEvent.ACTION_UP) {
			button.setBackgroundColor(Color.TRANSPARENT);
		} else if (event.getAction() == MotionEvent.ACTION_DOWN) {
			button.setBackgroundColor(getResources()
					.getColor(R.color.tabsColor));
		}
		return false;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.mostly_visted_button:
			ArticlesTab.CURRENT_STATE = ArticlesTab.ALL_ARTICLES_MOSTLY_VISITED;
			pager.setCurrentItem(1);
		case R.id.newest_articles_button:
			ArticlesTab.CURRENT_STATE = ArticlesTab.ALL_ARTICLES_NEWEST_ARTICLES;
			pager.setCurrentItem(1);
		case R.id.departements_button:
			pager.setCurrentItem(2);
		case R.id.authors_button:
			pager.setCurrentItem(3);
		case R.id.about_us_button:
			showDialog(v.getResources().getString(R.string.about_us_label));
		}
	}
	
	private void showDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void setPager(ViewPager pager) {
		this.pager = pager;
	}
	
	public void setToolbar(Toolbar toolbar) {
		this.toolbar = toolbar;
	}

}
