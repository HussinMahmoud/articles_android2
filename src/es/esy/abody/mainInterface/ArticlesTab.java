package es.esy.abody.mainInterface;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable; // her i get problem i want tel you see your maile now
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import es.esy.abody.articleClient.ArticleClient;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articles.R;

public class ArticlesTab extends Fragment {

	public static int CURRENT_STATE = 0;
	public static int NO_CONTENT = 0;
	public static int ALL_ARTICLES_MOSTLY_VISITED = 1;
	public static int ALL_ARTICLES_NEWEST_ARTICLES = 2;
	public static int ONE_ARTICLE = 3;
	public static int BY_DEPARTEMENT = 4;
	public static int BY_AUTHOR = 5;

	public static int CURRENT_DEPARTEMENT_ID = 0;
	public static int CURRENT_AUTHOR_ID = 0;
	public static ArticleVO CURRENT_ARTICLE;

	public static boolean FETCH_OR_NOT = false;

	private ViewPager pager;
	private Toolbar toolbar;
	private LayoutInflater inflater;
	private View v;
	private ArticlesListAdapter articlesAdapter;
	private ArrayList<ArticleVO> articlesList;
	private boolean isFetching = false;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		this.inflater = inflater;
		View v = inflater.inflate(R.layout.articles_tab, container, false);
		this.v = v;
		articlesList = new ArrayList<ArticleVO>();
		articlesAdapter = new ArticlesListAdapter(v.getContext(), articlesList);
		ListView list = (ListView) v.findViewById(R.id.articles_list);
		list.setAdapter(articlesAdapter);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int index, long arg3) {
				CURRENT_ARTICLE = articlesList.get(index);
				startArticleActivity();
			}

		});
		final Button showMore = (Button) v.findViewById(R.id.show_more_button);
		showMore.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					showMore.setBackgroundColor(getResources().getColor(
							R.color.tabsColor));
					showMore.setTextColor(Color.BLACK);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					showMore.setBackgroundColor(Color.BLACK);
					showMore.setTextColor(Color.WHITE);
				}
				return false;
			}
		});
		showMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View view) {

				new Thread() {
					@Override
					public void run() {
						try {
							int lastArticleId = articlesList.get(
									articlesList.size() - 1).getId();
							int lasrtArticleAuthor = articlesList.get(
									articlesList.size() - 1).getAuthor_id();
							int lastArticleDepartement = articlesList.get(
									articlesList.size() - 1).getDepartment_id();
							ArticleClient client = new ArticleClient(view
									.getContext());
							ArrayList<ArticleVO> articles = new ArrayList<ArticleVO>();
							boolean isThereData = false;
							if (CURRENT_STATE == ALL_ARTICLES_MOSTLY_VISITED) {
								isThereData = client
										.fetchOlderArticle(lastArticleId);
								articles = (ArrayList<ArticleVO>) (isThereData ? client
										.listArticleOrderByNewer() : articles);
							} else if (CURRENT_STATE == ALL_ARTICLES_NEWEST_ARTICLES) {
								isThereData = client
										.fetchOlderArticle(lastArticleId);
								articles = (ArrayList<ArticleVO>) (isThereData ? client
										.listArticleOrderByNewer() : articles);
							} else if (CURRENT_STATE == BY_AUTHOR) {
								isThereData = client.fetchOlderArticleToAuthor(
										lastArticleId, lasrtArticleAuthor);
								articles = (ArrayList<ArticleVO>) (isThereData ? client
										.listArticleByAuthorId(lasrtArticleAuthor)
										: articles);
							} else if (CURRENT_STATE == BY_DEPARTEMENT) {
								isThereData = client
										.fetchOlderArticleInDepartment(
												lastArticleId,
												lastArticleDepartement);
								articles = (ArrayList<ArticleVO>) (isThereData ? client
										.listArticleByDpartmetId(lastArticleDepartement)
										: articles);
							}
							if (isThereData) {
								articlesList = articles;
								view.post(new Runnable() {

									@Override
									public void run() {
										articlesAdapter.removeAllArticles();
										articlesAdapter
												.addArticles(articlesList);
									}
								});
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}

				}.start();

			}
		});
		return v;
	}

	private void startArticleActivity() {
		Intent intent = new Intent(v.getContext(), ArticleActivity.class);
		getActivity().startActivity(intent);
	}

	public void startFetching() {
		if (!FETCH_OR_NOT) {
			return;
		}
		ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_image,
				null);
		Animation rotation = AnimationUtils.loadAnimation(v.getContext(),
				R.anim.refresh_animation);
		rotation.setRepeatCount(Animation.INFINITE);
		iv.startAnimation(rotation);
		MenuItem m = toolbar.getMenu().findItem(R.id.action_refresh);
		MenuItemCompat.setActionView(m, iv);
		FetchArticlesTask task = new FetchArticlesTask();
		isFetching = true;
		task.execute("Fetch Articles");
	}

	public void stopFetching() {
		MenuItem m = toolbar.getMenu().findItem(R.id.action_refresh);
		if (MenuItemCompat.getActionView(m) != null && isFetching) {
			MenuItemCompat.getActionView(m).clearAnimation();
			MenuItemCompat.setActionView(m, null);
			isFetching = false;
		}
	}

	public void setPager(ViewPager pager) {
		this.pager = pager;
	}

	public void setToolbar(Toolbar toolbar) {
		this.toolbar = toolbar;
	}

	public int getArticlesListSize() {
		return this.articlesList.size();
	}

	class FetchArticlesTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			try {
				// ArticleClient client = new ArticleClient(v.getContext());
				if (CURRENT_STATE == NO_CONTENT) {
					ArticleClient client = new ArticleClient(v.getContext());
					articlesList = (ArrayList<ArticleVO>) client
							.listArticleOrderByNewer();
					v.post(new Runnable() {

						@Override
						public void run() {
							articlesAdapter.removeAllArticles();
							articlesAdapter.addArticles(articlesList);
						}
					});
					CURRENT_STATE = ALL_ARTICLES_NEWEST_ARTICLES;
				} else if (CURRENT_STATE == ALL_ARTICLES_MOSTLY_VISITED) {
					ArticleClient client = new ArticleClient(v.getContext());
					articlesList = (ArrayList<ArticleVO>) client
							.listArticleOrderByNewer();
					v.post(new Runnable() {

						@Override
						public void run() {
							articlesAdapter.removeAllArticles();
							articlesAdapter.addArticles(articlesList);
						}
					});
				} else if (CURRENT_STATE == ALL_ARTICLES_NEWEST_ARTICLES) {
					ArticleClient client = new ArticleClient(v.getContext());
					articlesList = (ArrayList<ArticleVO>) client
							.listArticleOrderByNewer();
					v.post(new Runnable() {

						@Override
						public void run() {
							articlesAdapter.removeAllArticles();
							articlesAdapter.addArticles(articlesList);
						}
					});
				} else if (CURRENT_STATE == BY_DEPARTEMENT) {
					ArticleClient client = new ArticleClient(v.getContext());
					articlesList = (ArrayList<ArticleVO>) client
							.listArticleByDpartmetId(CURRENT_DEPARTEMENT_ID);
					v.post(new Runnable() {

						@Override
						public void run() {
							articlesAdapter.removeAllArticles();
							articlesAdapter.addArticles(articlesList);
						}
					});
				} else if (CURRENT_STATE == BY_AUTHOR) {
					ArticleClient client = new ArticleClient(v.getContext());
					articlesList = (ArrayList<ArticleVO>) client
							.listArticleByAuthorId(CURRENT_AUTHOR_ID);
					v.post(new Runnable() {

						@Override
						public void run() {
							articlesAdapter.removeAllArticles();
							articlesAdapter.addArticles(articlesList);
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String data) {
			stopFetching();
		}

	}

}
