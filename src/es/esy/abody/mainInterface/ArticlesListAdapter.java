package es.esy.abody.mainInterface;

import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import es.esy.abody.articleServer.sharedVOs.ArticleVO;
import es.esy.abody.articles.R;

public class ArticlesListAdapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<ArticleVO> articlesList;

	public ArticlesListAdapter(Context context,
			ArrayList<ArticleVO> articlesList) {
		// super(context, R.layout.article_row);
		this.context = context;
		this.articlesList = articlesList;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View rowView = inflater.inflate(R.layout.article_row, null, true);
		final ImageView imageView = (ImageView) rowView
				.findViewById(R.id.article_icon);
		TextView articleTitle = (TextView) rowView
				.findViewById(R.id.article_title);
		new Thread() {

			@Override
			public void run() {
				try {
					URL imageUrl = new URL(articlesList.get(position)
							.getPhoto_url());
					final Bitmap articleImage = BitmapFactory
							.decodeStream(imageUrl.openConnection()
									.getInputStream());
					imageView.post(new Runnable() {
						@Override
						public void run() {
							imageView.setImageBitmap(articleImage);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}.start();
		TextView articleText = (TextView) rowView
				.findViewById(R.id.article_text);
		// Typeface type = Typeface.createFromAsset(rowView.getContext()
		// .getAssets(), "traditional.ttf");
		// articleText.setTypeface(type);
		articleTitle.setText(articlesList.get(position).getTitle());
		articleText.setText(articlesList.get(position).getDesc());
		return rowView;
	}

	public void addArticle(ArticleVO article) {
		articlesList.add(article);
		this.notifyDataSetChanged();
	}

	public void addArticles(ArrayList<ArticleVO> articles) {
		for (ArticleVO article : articles) {
			articlesList.add(article);
		}
		this.notifyDataSetChanged();
	}

	public void removeAllArticles() {
		articlesList.clear();
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return articlesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return articlesList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
