package es.esy.abody.mainInterface;

import java.io.UnsupportedEncodingException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import es.esy.abody.articleClient.ArticleClient;
import es.esy.abody.articles.R;

public class MainInterface extends ActionBarActivity {

	Toolbar toolbar;
	ViewPager pager;
	PagerAdapter adapter;
	SlidingTabLayout tabs;
	String[] Titles;
	int Numboftabs = 4;
	boolean buttonToggle = false;
	boolean isConnected = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_interface);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		Titles = new String[] { getResources().getString(R.string.home_tab),
				getResources().getString(R.string.articles_tab),
				getResources().getString(R.string.departements),
				getResources().getString(R.string.authors) };
		adapter = new PagerAdapter(getSupportFragmentManager(), Titles,
				Numboftabs);

		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		adapter.setPager(pager);
		adapter.setToolbar(toolbar);
		tabs = (SlidingTabLayout) findViewById(R.id.tabs);
		tabs.setDistributeEvenly(true);

		tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
			@Override
			public int getIndicatorColor(int position) {
				return getResources().getColor(R.color.tabsColor);
			}
		});
		tabs.setViewPager(pager);
		adapter.setSlidingLayout(tabs);
		try {
			final SharedPreferences pref = getSharedPreferences(
					"es.esy.abody.articles", MODE_PRIVATE);
			boolean isFirstRun = pref.getBoolean("first_run", true);
			final ArticleClient client = new ArticleClient(this);
			isConnected = false;
			final ProgressDialog dialog = ProgressDialog.show(MainInterface.this,
					"", getResources().getString(R.string.connecting_message));
			dialog.setCancelable(false);
			dialog.setIndeterminate(true);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.show();
			if (isFirstRun) {
				new Thread() {
					@Override
					public void run() {
						try {
							isConnected = client.startAppAndInsertFristFetch();
							if (isConnected) {
								pref.edit().putBoolean("first_run", false)
										.commit();
							}
							dialog.dismiss();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();
			} else {
				new Thread() {
					@Override
					public void run() {
						try {
							client.updateAppInsertInDb();
							dialog.dismiss();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();
				isConnected = true;
			}
			if (!isConnected) {
				showDialog(getResources().getString(
						R.string.internet_conn_error));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_interface_menu, menu);
		adapter.setToolbarListeners();
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_share:
			Intent intent = new Intent(Intent.ACTION_SEND);

			intent.setType("text/plain");

			intent.putExtra(Intent.EXTRA_TEXT,
					getResources().getString(R.string.website_link));

			intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					getResources().getString(R.string.website_message));

			startActivity(Intent.createChooser(intent, "Share"));
			return true;
		case R.id.action_menu:
			adapter.setView(findViewById(R.id.action_menu));
			adapter.setOptionsMenu();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public ViewPager getPager() {
		return pager;
	}

	private void showDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						System.exit(0);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
}
