package es.esy.abody.mainInterface;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import es.esy.abody.articleServer.sharedVOs.AuthorVO;
import es.esy.abody.articles.R;

public class AuthorsListAdapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<AuthorVO> authorsList;

	public AuthorsListAdapter(Context context, ArrayList<AuthorVO> authorsList) {
		// super(context, R.layout.article_row);
		this.context = context;
		this.authorsList = authorsList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return authorsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return authorsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if(authorsList.get(position).getId() == 0) {
			return null;
		}
		LayoutInflater inflater = LayoutInflater.from(context);
		View rowView = inflater.inflate(R.layout.author_row, null, true);
		TextView authorName = (TextView) rowView.findViewById(R.id.author_name);
		authorName.setText(authorsList.get(position).getName());
		return rowView;
	}

	public void addAuthor(AuthorVO author) {
		authorsList.add(author);
		this.notifyDataSetChanged();
	}

	public void addAuthors(ArrayList<AuthorVO> authors) {
		for (AuthorVO author : authors) {
			authorsList.add(author);
		}
		this.notifyDataSetChanged();
	}

	public void removeAllAuthors() {
		authorsList.clear();
		this.notifyDataSetChanged();
	}

}
