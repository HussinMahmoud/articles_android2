package es.esy.abody.mainInterface;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import es.esy.abody.articles.R;

public class PagerAdapter extends FragmentPagerAdapter {

	CharSequence Titles[];
	MainTab tab1;
	ArticlesTab tab2;
	DepartmentsTab tab3;
	AuthorsTab tab4;
	SlidingTabLayout slidingLayout;
	int NumbOfTabs;
	int position = 0;

	private ViewPager pager;
	private Toolbar toolbar;
	private View view;

	public PagerAdapter(FragmentManager fm, CharSequence mTitles[],
			int mNumbOfTabsumb) {
		super(fm);

		this.Titles = mTitles;
		this.NumbOfTabs = mNumbOfTabsumb;

	}

	@Override
	public Fragment getItem(int position) {

		if (position == 0) {
			tab1 = new MainTab();
			tab1.setToolbar(toolbar);
			tab1.setPager(pager);
			return tab1;
		} else if (position == 1) {
			tab2 = new ArticlesTab();
			tab2.setToolbar(toolbar);
			// tab2.startFetching();
			tab2.setPager(pager);
			return tab2;
		} else if (position == 2) {
			tab3 = new DepartmentsTab();
			tab3.setToolbar(toolbar);
			tab3.setPager(pager);
			return tab3;
		} else {
			tab4 = new AuthorsTab();
			tab4.setToolbar(toolbar);
			tab4.setPager(pager);
			return tab4;
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return Titles[position];
	}

	@Override
	public int getCount() {
		return NumbOfTabs;
	}

	public void setPager(ViewPager pager) {
		this.pager = pager;
	}

	public void setToolbar(final Toolbar toolbar) {
		this.toolbar = toolbar;
	}

	public void setView(View view) {
		this.view = view;
	}

	public void setSlidingLayout(SlidingTabLayout slidingLayout) {
		this.slidingLayout = slidingLayout;
		this.slidingLayout
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						super.onPageSelected(position);
						setPosition(position);
						if (position == 0) {
							tab2.stopFetching();
							tab3.stopFetching();
							tab4.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(false);
						} else if (position == 1) {
							tab3.stopFetching();
							tab4.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							ArticlesTab.FETCH_OR_NOT = tab2
									.getArticlesListSize() < 1;
							tab2.startFetching();
						} else if (position == 2) {
							tab2.stopFetching();
							tab4.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							DepartmentsTab.FETCH_OR_NOT = tab3.getContentSize() < 1;
							tab3.startFetching();
						} else if (position == 3) {
							tab2.stopFetching();
							tab3.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							AuthorsTab.FETCH_OR_NOT = tab4.getContentSize() < 1;
							tab4.startFetching();
						}
					}
				});
	}

	public void setToolbarListeners() {
		MenuItem refresh_button = this.toolbar.getMenu().findItem(
				R.id.action_refresh);
		refresh_button
				.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem arg0) {
						if (position == 1) {
							tab3.stopFetching();
							tab4.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							tab2.stopFetching();
							ArticlesTab.CURRENT_STATE = ArticlesTab.NO_CONTENT;
							tab2.startFetching();
						} else if (position == 2) {
							tab2.stopFetching();
							tab4.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							tab3.stopFetching();
							DepartmentsTab.CURRENT_STATE = DepartmentsTab.NO_CONTENT;
							tab3.startFetching();
						} else if (position == 3) {
							tab2.stopFetching();
							tab3.stopFetching();
							toolbar.getMenu().findItem(R.id.action_refresh)
									.setVisible(true);
							tab3.stopFetching();
							AuthorsTab.CURRENT_STATE = AuthorsTab.NO_CONTENT;
							tab4.startFetching();
						}
						return false;
					}
				});
	}

	private void setPosition(int position) {
		this.position = position;
	}

	public void setOptionsMenu() {
		if (position == 1) {
			final PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
			MenuInflater inflate = popupMenu.getMenuInflater();
			inflate.inflate(R.menu.options_menu, popupMenu.getMenu());
			popupMenu
					.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

						@Override
						public boolean onMenuItemClick(MenuItem item) {
							if (item == popupMenu.getMenu().findItem(
									R.id.action_mostly_visited)) {
								tab2.stopFetching();
								ArticlesTab.CURRENT_STATE = ArticlesTab.ALL_ARTICLES_MOSTLY_VISITED;
								tab2.startFetching();
							} else if (item == popupMenu.getMenu().findItem(
									R.id.action_newest)) {
								tab2.stopFetching();
								ArticlesTab.CURRENT_STATE = ArticlesTab.ALL_ARTICLES_NEWEST_ARTICLES;
								tab2.startFetching();
							}
							return false;
						}
					});
			popupMenu.show();
		}
	}

}
